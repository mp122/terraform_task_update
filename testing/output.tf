output "pubip" {
  value = azurerm_public_ip.main.ip_address
}
variable "abc" {
  default = "${"pubip.value"}"
}

resource "local_file" "foo" {
  content  = "${var.abc}"
  filename = "${path.module}/inv.txt"
}
